﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class EffectsCanvas : MonoBehaviour {
    public GameObject autoAim;
    public GameObject power;
    public GameObject freeze;
    public GameObject levelUp;

    public void EnablePanel(GameObject effectPanel, bool active)
    {
        effectPanel.SetActive(active);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
