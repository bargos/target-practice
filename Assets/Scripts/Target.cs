﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class Target : MonoBehaviour {
    public enum TargetType
    {
        Default,
        AutoAim,
        Power,
        Freeze,
        Money
    }

    public float hitPoints = 1;
    public TargetType type = TargetType.Default;

    public GameObject brokenTargetPrefab;

    Vector3 initialSize;
    private void Start()
    {
        // weird
        if (GetComponentInChildren<TextMeshPro>() != null) GetComponentInChildren<TextMeshPro>().text = hitPoints.ToString();


        initialSize = transform.localScale;

        InitializeType();
    }
    void InitializeType()
    {
        switch (type)
        {
            case TargetType.Default:
                break;
            case TargetType.AutoAim:
                if (GetComponentInChildren<TextMeshPro>() != null) GetComponentInChildren<TextMeshPro>().text = "A";
                hitPoints = 1;
                break;
            case TargetType.Power:
                if (GetComponentInChildren<TextMeshPro>() != null) GetComponentInChildren<TextMeshPro>().text = "P";
                hitPoints = 1;
                break;
            case TargetType.Freeze:
                if (GetComponentInChildren<TextMeshPro>() != null) GetComponentInChildren<TextMeshPro>().text = "F";
                hitPoints = 1;
                break;
            case TargetType.Money:
                break;
        }
    }
    

    private void FixedUpdate()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, initialSize, 0.3f);
    }

    public void TakeDamage(float damage)
    {
        hitPoints -= damage;

        UIManager.instance.addScore((int)damage);

        // update damage text
        if (type == TargetType.Default) { if (GetComponentInChildren<TextMeshPro>() != null) GetComponentInChildren<TextMeshPro>().text = hitPoints.ToString(); } 

        transform.localScale = initialSize * 0.6f;

        if(hitPoints <= 0)
        {
            Break();
        }
    }
    void Break()
    {
        Destroy(GetComponentInChildren<TextMeshPro>());

        GameObject broken = Instantiate(brokenTargetPrefab, transform.position, transform.rotation);
        Material myMat = GetComponentInChildren<MeshRenderer>().material;

        foreach(Rigidbody rb in broken.GetComponentsInChildren<Rigidbody>())
        {
            Vector3 force = Random.insideUnitSphere * 2 * 0.02f + Vector3.up * 10 * 1f;
            force += -Vector3.forward * 100;
            Vector3 torque = Random.insideUnitSphere * 10 * 0.01f;
            rb.AddForce(force);
            rb.AddTorque(torque);

            if(rb.GetComponent<MeshRenderer>() != null)
            {
                rb.GetComponent<MeshRenderer>().material = myMat;
            }
        }


        Weapon weapon = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Weapon>();
        switch (type)
        {
            case TargetType.Default:
                break;
            case TargetType.AutoAim:
                weapon.StartPowerUp(Weapon.PowerUps.AutoAim);
                break;
            case TargetType.Power:
                weapon.StartPowerUp(Weapon.PowerUps.Power);
                break;
            case TargetType.Freeze:
                weapon.StartPowerUp(Weapon.PowerUps.Freeze);
                break;
            case TargetType.Money:
                break;
        }

        Destroy(gameObject);
    }

}
