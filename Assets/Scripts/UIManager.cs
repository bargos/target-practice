﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {


	public static UIManager instance;

    public GameObject mainMenuPanel;

    public Text moneyText;
    public Text instantScoreTxt;
    public Text gameDurationText;

    public GameObject earnMoneyPopUp;
    public Text popupPanelDescriptionText;
	public Text earnedMoneyTxt;

    private bool showingIdlePrizePanel = false;

    private bool isManagerInitialized = false;
    private double idleWinningPrize = 0;
    private bool isLastRewardCollected = false;


    private int instantScore;
    private float gameDuration = 0;

    void Awake()
    {
        instance = this;
    }
	// Use this for initialization
	void Start () 
	{
        //calculateIdleWinningPrize();
        isManagerInitialized = true;
    }

    private void Update()
    {
        if(GameManager.instance.gameState == GameStates.IN_GAME)
        {
            gameDuration += Time.deltaTime;
            gameDurationText.text = "Duration : " + gameDuration.ToString("#.0");
        }
    }


    public void startGame()
    {
        gameDuration = 0;
        instantScore = 0;
        instantScoreTxt.text = "0";
        mainMenuPanel.SetActive(false);
    }

    public void gameOver()
    {
        gameDurationText.text = "Duration : 0";
        ShowEarnedMoneyPopUp();
    }


    public void addScore(int addScore)
	{
		instantScore += addScore;
		instantScoreTxt.text = "" + instantScore.ToString ("N0");
	}

    public void showIdlePrizeMoneyPopUp()
    {
        showingIdlePrizePanel = true;
        popupPanelDescriptionText.text = "Offline Earning";
        instantScoreTxt.text = "";
        earnedMoneyTxt.text = "$" + idleWinningPrize.ToString("N0");
        earnMoneyPopUp.SetActive(true);
    }


    public void ShowEarnedMoneyPopUp() // round bittiğinde çağırılıyor
	{
        showingIdlePrizePanel = false;
        popupPanelDescriptionText.text = "Nice Shoot !";
        instantScoreTxt.text = "";
		earnedMoneyTxt.text = "$"+instantScore.ToString ("N0");
		earnMoneyPopUp.SetActive (true);
	}


	public void collectMoney(bool isDouble)
	{
        if(showingIdlePrizePanel)
        {
            isLastRewardCollected = true;
            if (isDouble)
            {
                idleWinningPrize *= 2;
            }
            UpgradeManager.intance.earnMoney(idleWinningPrize);
            earnMoneyPopUp.SetActive(false);
        }
        else
        {
            if (isDouble) { 
                instantScore *= 2;
            }
            UpgradeManager.intance.earnMoney(instantScore);
            earnMoneyPopUp.SetActive(false);
            /*
            if (!isDouble)
            {
                if (ShowInterstitialScript.instance.canShowAd())
                {
                    ShowInterstitialScript.instance.showAd();
                }
                else
                {
                    RateUs.instance.GameOver();
                }
            }
            else
            {
                RateUs.instance.GameOver();
            }
            */
        }
        mainMenuPanel.SetActive(true);
    }

    public void setMoney(double amount)
    {
        moneyText.text = "$"+amount.ToString("N0");
    }


    private void calculateIdleWinningPrize()
    {
        if (LastOpenDate == DateTime.MinValue)
        {
            return;
        }
        double totalPassedSeconds = 0;
        totalPassedSeconds = (DateTime.Now - LastOpenDate).TotalSeconds;

        Debug.Log(" $$$ totalPassedSeconds : " + totalPassedSeconds);

        if (totalPassedSeconds >= 86400) // max 1 days
        {
            totalPassedSeconds = 86400;
        }
        else if (totalPassedSeconds < 0)
        {
            totalPassedSeconds = 0;
        }

        idleWinningPrize = UpgradeManager.intance.getIdleWinningForOneSecond() * ((totalPassedSeconds));
        //idleWinningPrize = 11;
        if (idleWinningPrize >= 5)
        {
            showIdlePrizeMoneyPopUp();
        }

    }

    public DateTime LastOpenDate
    {
        get
        {
            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(PlayerPrefs.GetString("LastOpenDate", DateTime.MinValue.ToString()), out dt))
            {
                return dt;
            }
            return DateTime.MinValue;
        }
        set
        {
            PlayerPrefs.SetString("LastOpenDate", value.ToString());
        }
    }

    public void saveLastState()
    {
        if (!isManagerInitialized)
        {
            return;
        }
        if (isLastRewardCollected || LastOpenDate.Equals(DateTime.MinValue))
        {
            LastOpenDate = DateTime.Now;
        }
    }

    void OnApplicationQuit()
    {
        OnApplicationPause(true);
    }

    void OnApplicationPause(bool pauseStatus)
    {

        if (pauseStatus)
        {
            saveLastState();
        }
    }



}
