﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Circle : MonoBehaviour {
    public WaveController.TargetInfo[] targets;
    public enum CircleAnimation
    {
        Stationary,
        Right,
        Left,
        Flip,
        Pause
    }
    public CircleAnimation circleAnimation = CircleAnimation.Stationary;

    public Transform circleShape;
    float radius = 6;

    bool adaptiveSpeed = false;
    public void InitializeCircle()
    {
        //GetComponent<Rigidbody>().velocity = -Vector3.forward * speed;
        GameObject backPanel = GameObject.FindGameObjectWithTag("BackPanel");

        //Vector3 startPos = backPanel.transform.position - Vector3.forward * 20;
        //transform.position = startPos;

        Vector3 scale = circleShape.transform.localScale;
        scale.x *= 0.8f;
        circleShape.transform.localScale = scale;

        SpawnTargets();
    }

    private void FixedUpdate()
    {
        int howManyTargetsLeft = 0;
        foreach (Target target in spawnedTargets)
        {
            if (target != null)
            {
                //Destroy(target.gameObject, 0.1f);
                howManyTargetsLeft++;
            }
        }
        if(howManyTargetsLeft == 0) { /*speed *= 1.05f;*/ }

        AnimateTargets();
    }

    int posesOnCircle = 14;
    List<Target> spawnedTargets;
    int[] initialOnCircle;
    Target[] targetsToAnimamate;
    public void SpawnTargets()
    {
        bool[] occupiedOnCircle = new bool[posesOnCircle];
        for(int i = 0; i < posesOnCircle; i++) { occupiedOnCircle[i] = false; }

        spawnedTargets = new List<Target>();
        initialOnCircle = new int[posesOnCircle];
        previousOnCircleRadians = new float[posesOnCircle];
        for(int i = 0; i < targets.Length; i++)
        {
            float radiansPerPos = (Mathf.PI * 2 / posesOnCircle);

            int onCircle = Random.Range(0, posesOnCircle);
            initialOnCircle[i] = onCircle;

            int whileIterations = 20;
            while(occupiedOnCircle[onCircle] == true) { if (whileIterations < 0) { print("while saved from infinite loop"); break; } onCircle++; onCircle = onCircle % posesOnCircle; whileIterations--;  }
            float onCircleRadians = onCircle * radiansPerPos;
            previousOnCircleRadians[i] = onCircleRadians;
            //Vector3 pos = Random.insideUnitCircle.normalized * radius;
            Vector3 pos = new Vector2(Mathf.Cos(onCircleRadians), Mathf.Sin(onCircleRadians)) * radius;

            pos.x *= 0.8f;
            //Target newTarget = Instantiate(targets[i], pos + transform.position, Quaternion.identity).GetComponent<Target>();
            Target newTarget = WaveController.ConstructTarget(targets[i], this);
            newTarget.transform.position = pos + transform.position + -(Vector3.forward * 0.1f);

            newTarget.transform.SetParent(transform, true);
            spawnedTargets.Add(newTarget);

            occupiedOnCircle[onCircle] = true;
        }
        targetsToAnimamate = GetComponentsInChildren<Target>();
        StartCoroutine(CircleCoroutine());
    }

    IEnumerator CircleCoroutine()
    {
        Transform endPanel = GameObject.FindGameObjectWithTag("EndPanel").transform;
        while (transform.position.z > endPanel.transform.position.z)
        {
            yield return new WaitForSeconds(0.5f);
        }


        int howManyTargetsLeft = 0;
        foreach(Target target in spawnedTargets)
        {
            if(target != null)
            {
                Destroy(target.gameObject, 0.1f);
                howManyTargetsLeft++;
            }
        }
        Destroy(gameObject, 0.2f);
        print("missed targets: " + howManyTargetsLeft);
        // clea3 missed targets


        yield return null;
    }
    float[] previousOnCircleRadians;
    void AnimateTargets()
    {
        for (int i = 0; i < targetsToAnimamate.Length; i++)
        {
            float radiansPerPos = (Mathf.PI * 2 / posesOnCircle);

            int onCircle = initialOnCircle[i];

            float oldOnCircleRadians = previousOnCircleRadians[i];
            float onCircleRadians = oldOnCircleRadians;
            switch (circleAnimation)
            {
                case CircleAnimation.Stationary:
                    
                    break;
                case CircleAnimation.Right:
                    onCircleRadians += Time.fixedDeltaTime * 0.5f;
                    break;
                case CircleAnimation.Left:
                    onCircleRadians -= Time.fixedDeltaTime * 0.5f;
                    break;
                case CircleAnimation.Flip:
                    if(Time.time % 6 > 3)
                    {
                        onCircleRadians += Time.fixedDeltaTime * 0.5f;
                    }
                    else
                    {
                        onCircleRadians -= Time.fixedDeltaTime * 0.5f;
                    }
                    break;
                case CircleAnimation.Pause:
                    if (Time.time % 6 > 3)
                    {
                        onCircleRadians += Time.fixedDeltaTime * 0.5f;
                    }
                    break;
            }


            

            //Vector3 pos = Random.insideUnitCircle.normalized * radius;
            Vector3 pos = new Vector2(Mathf.Cos(onCircleRadians), Mathf.Sin(onCircleRadians)) * radius;

            pos.x *= 0.8f;

            previousOnCircleRadians[i] = onCircleRadians;
            if (targetsToAnimamate[i] != null) { targetsToAnimamate[i].transform.position = pos + transform.position + -(Vector3.forward * 0.1f); }

        }
    }
}
