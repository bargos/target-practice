﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {
    public float speed; // gun speed though the level
    public WaveController.CircleInfo[] circlesList;

    public bool levelClear = false;

    public float seperationBetweenCircles = 5;


    float startTime;
    private void Start()
    {
        StartCoroutine(LevelCoroutine());
        startTime = Time.time;
    }
    private void FixedUpdate()
    {
        /*
        if(Time.time < startTime + 10)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 12, 0.1f);
        }
        else
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, 1, 0.1f);
        }

        */

    }
    private void Update()
    {
        // move the gun
        Transform player = GameObject.FindGameObjectWithTag("Player").transform;
        //player.transform.Translate(Time.deltaTime * speed * Vector3.forward);
        player.GetComponentInChildren<Weapon>().levelMultiplier = Mathf.Lerp(player.GetComponentInChildren<Weapon>().levelMultiplier, speed, 0.1f);
        

        if (levelClear) { Destroy(gameObject); }
    }


    IEnumerator LevelCoroutine()
    {
        float circleZPos = 0;

        Circle lastCircle = null;
        for(int i = 0; i < circlesList.Length; i++)
        {
            //Circle newCircle = Instantiate(circlesList[i], transform.position, Quaternion.identity).GetComponent<Circle>();
            Circle newCircle = WaveController.ConstructCircle(circlesList[i], this);
            newCircle.InitializeCircle();
            newCircle.transform.position += Vector3.forward * circleZPos;

            lastCircle = newCircle;

            circleZPos += seperationBetweenCircles;
            //yield return new WaitForFixedUpdate();
        }

        Transform endPanel = GameObject.FindGameObjectWithTag("EndPanel").transform;
        while (lastCircle != null)
        {
            yield return new WaitForSeconds(0.5f);
        }

        print("level clear");
        levelClear = true;
        
        yield return null;
    }
}
