﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {
    public Weapon controlledWeapon;
    public GameObject cursor;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        currentMousePos = Vector2.Lerp(currentMousePos, Input.mousePosition, 0.5f);

        RaycastHit mouseRaycast = RaycastToWorld();
        controlledWeapon.AimAt(mouseRaycast.point);

        //.transform.position = controlledWeapon.CurrentAimDir; //cursor.transform.position = mouseRaycast.point;
        Vector3 deltaToCam = controlledWeapon.CurrentAimDir - Camera.main.transform.position;
        cursor.transform.position = deltaToCam.normalized + Camera.main.transform.position;

        //float distance = (Camera.main.transform.position - cursor.transform.position).magnitude;
        //cursor.transform.localScale = Vector3.one * distance * 0.1f;

    }
    
    Vector2 currentMousePos;
    RaycastHit RaycastToWorld()
    {


        Ray ray = Camera.main.ScreenPointToRay((Vector3)currentMousePos * 2f - new Vector3(Screen.width, Screen.height * 0) * 0.5f);
        RaycastHit[] hits = Physics.RaycastAll(ray, 100000);
        Debug.DrawRay(ray.origin, ray.direction * 1000, Color.magenta, 0.1f);

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.gameObject.CompareTag("Target"))
            {
                return hits[i];
            }
        }
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.gameObject.CompareTag("BackPanel"))
            {
                return hits[i];
            }
        }

        return hits[0];
        if(hits.Length != 0) { print("hit: " + hits[0].transform.gameObject); return hits[0]; }
        print("PROBLEM");
        return (new RaycastHit());
    }

    
}
