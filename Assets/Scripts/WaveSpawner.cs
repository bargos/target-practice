﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour {
    WaveController.LevelInfo[] levels;
    GameObject backPanel;

    public static WaveSpawner instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        backPanel = GameObject.FindGameObjectWithTag("BackPanel");

        //GenerateLevelExample();
    }

    public void GenerateLevelExample()
    {

        levels = new WaveController.LevelInfo[3];
        for (int l = 0; l < 3; l++)
        {
            WaveController.LevelInfo testLevel = new WaveController.LevelInfo();

            testLevel.seperationBetweenCircles = 15f;
            testLevel.speed = 5;


            testLevel.circles = new WaveController.CircleInfo[7];
            for (int i = 0; i < testLevel.circles.Length; i++)
            {
                testLevel.circles[i] = new WaveController.CircleInfo();
                testLevel.circles[i].circleAnimation = (Circle.CircleAnimation)Random.Range(0, System.Enum.GetNames(typeof(Circle.CircleAnimation)).Length);

                int targetCount = 6;
                if (i < 1) { targetCount = 0; } // no targets on first circle
                testLevel.circles[i].targets = new WaveController.TargetInfo[targetCount];
                for (int j = 0; j < testLevel.circles[i].targets.Length; j++)
                {



                    testLevel.circles[i].targets[j] = new WaveController.TargetInfo();
                    testLevel.circles[i].targets[j].hitPoints = Random.Range(1, 1 + (int)(i / 2));
                    if (Random.Range(0, 10) >= 9) { testLevel.circles[i].targets[j].type = Target.TargetType.AutoAim; }
                    else if (Random.Range(0, 10) >= 9) { testLevel.circles[i].targets[j].type = Target.TargetType.Power; }
                    else if (Random.Range(0, 10) >= 9) { testLevel.circles[i].targets[j].type = Target.TargetType.Freeze; }
                    else { testLevel.circles[i].targets[j].type = Target.TargetType.Default; }
                }
            }

            levels[l] = testLevel;

            //ConstructLevel(testLevel);

        }
        StartCoroutine(LevelsCrt(levels));
    }


    IEnumerator LevelsCrt(WaveController.LevelInfo[] levelInfos)
    {
        print("levels start");
        float startZ = backPanel.transform.position.z + 50;

        for (int levelIndex = 0; levelIndex < levelInfos.Length; levelIndex++)
        {
            yield return LevelCrt(levelInfos[levelIndex], startZ);
            //startZ += 
            GameObject.FindGameObjectWithTag("EffectsCanvas").GetComponent<EffectsCanvas>().levelUp.SetActive(true);
            yield return new WaitForSeconds(2);
            GameObject.FindGameObjectWithTag("EffectsCanvas").GetComponent<EffectsCanvas>().levelUp.SetActive(false);
            // level finished
        }


        print("levels finished");
        yield return null;
    }
    IEnumerator LevelCrt(WaveController.LevelInfo levelInfo, float spawnZ)
    {
        LevelSlider levelSlider = GameObject.FindGameObjectWithTag("LevelSlider").GetComponent<LevelSlider>();
        Level level = WaveController.ConstructLevel(levelInfo);
        level.transform.position = backPanel.transform.position;
        float startPos = level.transform.position.z;

        /*
        // find endCircle
        Circle endCircle = null;
        foreach(Circle circle in level.GetComponentsInChildren<Circle>())
        {
            if (endCircle == null) endCircle = circle;
            if(circle.transform.position.z > endCircle.transform.position.z)
            {
                endCircle = circle;
            }
        }
        float endPos = endCircle.transform.position.z;
        */
        float endPos = levelInfo.circles.Length * levelInfo.seperationBetweenCircles + startPos;

        while (level.levelClear == false)
        {
            // update bar
            float playerPos = GameObject.FindGameObjectWithTag("BackPanel").transform.position.z;
            
            float percentage = ((playerPos - startPos) / (endPos - startPos));
            levelSlider.UpdateSlider(percentage);

            yield return new WaitForSeconds(0.05f);
        }
        levelSlider.UpdateSlider(0, levelSlider.GetCurrentLevel() + 1);
        print("level finished");
        yield return null;
    }
    IEnumerator LevelFinishCrt()
    {
        Target levelFinishTarget = Instantiate(WaveController.levelFinishTargetPrefab, GameObject.FindGameObjectWithTag("BackPanel").transform.position, Quaternion.identity);
        while (levelFinishTarget != null)
        {
            levelFinishTarget.transform.position += -Vector3.forward * 0.1f;
            yield return new WaitForFixedUpdate();
        }

        print("level finish target destroyed");

        yield return null;
    }
}
public static class AdjustValues
{
    public static float autoAimDuration = 5;

    public static float powerDuration = 5;
    public static float powerDamageMultiplier = 2;

    public static float freezeDuration = 5; // unscaled time
    public static float freezeMultiplier = 0.5f;

    public static float weaponDamage = 1;
    public static float weaponFireRate = 2;
}