﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour
{


    public Button upgrade_1_button;
    public Button upgrade_2_button;
    public Button upgrade_3_button;

    public Text upgrade_1_value_text;
    public Text upgrade_2_value_text;
    public Text upgrade_3_value_text;

    public Text upgrade_1_upgrade_value_text;
    public Text upgrade_2_upgrade_value_text;
    public Text upgrade_3_upgrade_value_text;

    public double MONEY
    {
        get
        {
            return double.Parse(PlayerPrefs.GetString("MONEY", "0"));
        }
        set
        {
            PlayerPrefs.SetString("MONEY", System.Math.Truncate(value).ToString());
        }
    }

    public int UPGRADE_1_LEVEL
    {
        get
        {
            return PlayerPrefs.GetInt("UPGRADE_1_LEVEL", 1);
        }
        set
        {
            PlayerPrefs.SetInt("UPGRADE_1_LEVEL", value);
        }
    }

    public int UPGRADE_2_LEVEL
    {
        get
        {
            return PlayerPrefs.GetInt("UPGRADE_2_LEVEL", 1);
        }
        set
        {
            PlayerPrefs.SetInt("UPGRADE_2_LEVEL", value);
        }
    }

    public int UPGRADE_3_LEVEL
    {
        get
        {
            return PlayerPrefs.GetInt("UPGRADE_3_LEVEL", 1);
        }
        set
        {
            PlayerPrefs.SetInt("UPGRADE_3_LEVEL", value);
        }
    }

    private double localMoney;
    public static UpgradeManager intance;
    // Use this for initialization
    private void Awake()
    {
        intance = this;
    }
    void Start()
    {
        localMoney = MONEY;
        UIManager.instance.setMoney(localMoney);
        checkButtonStates();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            earnMoney(100000);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            PlayerPrefs.DeleteAll();
        }
    }

    public void earnMoney(double amount)
    {
        localMoney += amount;
        checkButtonStates();
    }

    public void checkButtonStates()
    {
        MONEY = localMoney;
        UIManager.instance.setMoney(localMoney);

        upgrade_1_button.interactable = (localMoney >= getUpgradeValue(1));
        upgrade_1_value_text.text = ((getDisplayValue(1))).ToString("N0");
        upgrade_1_upgrade_value_text.text = "$"+getUpgradeValue(1).ToString("N0");

        upgrade_2_button.interactable = (localMoney >= getUpgradeValue(2));
        upgrade_2_value_text.text = ((getDisplayValue(2))).ToString("N0") + " s/bullet";
        upgrade_2_upgrade_value_text.text = "$" + getUpgradeValue(2).ToString("N0");

        upgrade_3_button.interactable = (localMoney >= getUpgradeValue(3));
        upgrade_3_value_text.text = "+$"+((getDisplayValue(3))).ToString("N0") + "/min";
        upgrade_3_upgrade_value_text.text = "$" + getUpgradeValue(3).ToString("N0");
        
    }

    public void upgradeButtonClicked(int upgradeType)
    {
        double requiredMoney = getUpgradeValue(1);
        switch (upgradeType)
        {
            case 1:
                requiredMoney = getUpgradeValue(1);
                if (localMoney >= requiredMoney)
                {
                    localMoney -= requiredMoney;
                    UPGRADE_1_LEVEL++;
                    checkButtonStates();
                }
                break;
            case 2:
                requiredMoney = getUpgradeValue(2);
                if (localMoney >= requiredMoney)
                {
                    localMoney -= requiredMoney;
                    UPGRADE_2_LEVEL++;
                    checkButtonStates();
                }
                break;
            case 3:
                requiredMoney = getUpgradeValue(3);
                if (localMoney >= requiredMoney)
                {
                    localMoney -= requiredMoney;
                    UPGRADE_3_LEVEL++;
                    checkButtonStates();
                }
                break;
        }
    }


    public double getIdleWinningForOneSecond()
    {
        if(UPGRADE_3_LEVEL <= 6)
        {
            return 762d / 86400;
        }else if (UPGRADE_3_LEVEL <= 9)
        {
            return 960d / 86400;
        }
        else
        {
            return getUpgradeValue(UPGRADE_3_LEVEL + 1) / 86400;
        }
    }

    public double getUpgradeValue(int upgradeType)
    {
        int levelToUse = UPGRADE_1_LEVEL;
        if (upgradeType == 2)
        {
            levelToUse = UPGRADE_2_LEVEL;
        }
        else if (upgradeType == 3)
        {
            levelToUse = UPGRADE_3_LEVEL;
        }

        return System.Math.Round(95.135d * (System.Math.Exp(0.2311d * levelToUse)));
    }

    public int getDisplayValue(int upgradeType)
    {
        switch (upgradeType)
        {
            case 1:
                return 20 + 10 * UPGRADE_1_LEVEL;
            case 2:
                return 2 + UPGRADE_2_LEVEL;
            case 3:
                if(UPGRADE_3_LEVEL == 1)
                {
                    return 3;
                }else if (UPGRADE_3_LEVEL == 2)
                {
                    return 4;
                }
                return (int)System.Math.Round(2.55d * (System.Math.Exp(0.2197d * UPGRADE_3_LEVEL)));
        }
        return 1;
    }

    public int GetMeter() // kaç metre dalacak
    {
        return getDisplayValue(1);
    }

    public int GetFishCapacity()// en fazla kaç balık tutabilir;
    {
        return getDisplayValue(2);
    }



    // TEST PART

    public void testDeleteAllClicked()
    {
        PlayerPrefs.DeleteAll();
        localMoney =0;
        checkButtonStates();
    }

    public void testGetMoneyClicked()
    {
        localMoney += getUpgradeValue(1)+getUpgradeValue(2)+getUpgradeValue(2);
        checkButtonStates();
    }
}
