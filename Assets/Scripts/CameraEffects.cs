﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEffects : MonoBehaviour {
    float initialFOV;
	// Use this for initialization
	void Start () {
        initialFOV = GetComponent<Camera>().fieldOfView;
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Camera>().fieldOfView = Mathf.Lerp(GetComponent<Camera>().fieldOfView, initialFOV, Time.deltaTime);

    }
    public void CameraShake(float multiplier = 1.15f)
    {
        GetComponent<Camera>().fieldOfView = initialFOV * multiplier;
    }
}
