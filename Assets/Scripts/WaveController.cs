﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour {
    #region Don't touch these
    public Level  setLevelPrefab;
    public Circle setCirclePrefab;
    public Target setTargetPrefab;
    public Target setLevelFinishTargetPrefab;

    static Level levelPrefab;
    static Circle circlePrefab;
    static Target targetPrefab;
    public static Target levelFinishTargetPrefab;

    #endregion

    #region Info classes
    public class TargetInfo
    {
        public float hitPoints;
        public Target.TargetType type;

        //public GameObject brokenTargetPrefab; // all meshes same?
    }

    public class CircleInfo
    {
        public TargetInfo[] targets;
        public Circle.CircleAnimation circleAnimation;

    }
    public class LevelInfo
    {
        public CircleInfo[] circles;
        public float speed; // circle move speed
        public float seperationBetweenCircles;
    }
    #endregion

    


    static GameObject backPanel;
    private void Start()
    {
        levelPrefab = setLevelPrefab;
        circlePrefab = setCirclePrefab;
        targetPrefab = setTargetPrefab;
        levelFinishTargetPrefab = setLevelFinishTargetPrefab;


        backPanel = GameObject.FindGameObjectWithTag("BackPanel");
    }
    public static Level ConstructLevel(LevelInfo levelInfo)
    {
        backPanel = GameObject.FindGameObjectWithTag("BackPanel");
        Level newLevel = Instantiate(levelPrefab, backPanel.transform.position, Quaternion.identity);
        newLevel.circlesList = levelInfo.circles;

        newLevel.speed = levelInfo.speed;
        newLevel.seperationBetweenCircles = levelInfo.seperationBetweenCircles;

        return newLevel;
    }
    public static Circle ConstructCircle(CircleInfo circleInfo, Level parent)
    {
        Circle newCircle = Instantiate(circlePrefab, backPanel.transform.position, Quaternion.identity);
        newCircle.targets = circleInfo.targets;
        newCircle.transform.SetParent(parent.transform);
        newCircle.transform.localPosition = Vector3.zero;
        newCircle.circleAnimation = circleInfo.circleAnimation;
        

        return newCircle;
    }
    public static Target ConstructTarget(TargetInfo targetInfo, Circle parent)
    {
        Target newTarget = Instantiate(targetPrefab, backPanel.transform.position, Quaternion.identity);
        newTarget.transform.SetParent(parent.transform);


        newTarget.hitPoints = targetInfo.hitPoints;
        newTarget.type = targetInfo.type;


        return newTarget;
    }

    
}
