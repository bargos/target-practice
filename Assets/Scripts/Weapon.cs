﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    Vector3 initialLocalPos;
    public float damage = 1f;
    float initialDamage;

    public float fireRate = 2;
    float initialFireRate;
    float shootCooldown;
    float readyToShoot;

    public GameObject gunShotEffect;
    public GameObject bulletImpactEffect;
    public Transform muzzle;

    bool disableControl = false;
    // Use this for initialization
    void Start()
    {
        damage = AdjustValues.weaponDamage;
        fireRate = AdjustValues.weaponFireRate;

        initialFireRate = fireRate;
        initialDamage = damage;
        initialLocalPos = transform.localPosition;
        shootCooldown = 1 / fireRate;
    }

    /// <summary>
    /// set this value from level
    /// </summary>
    public float levelMultiplier = 1;
    float powerMultiplier = 1;
    public void UpdateForwardMovement()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        Vector3 playerPos = player.transform.position;
        playerPos.z += levelMultiplier * powerMultiplier * Time.deltaTime;

        player.transform.position = playerPos;
    }
    private void Update()
    {
        GetComponent<Rigidbody>().isKinematic = false;

        UpdateMovement();
    }
    void FixedUpdate()
    {

        Debug.DrawRay(transform.position, transform.forward * 10000, Color.red, 0.1f);
        Debug.DrawLine(transform.position + Vector3.up * 0.1f, currentAim, Color.yellow, 0.1f);

        UpdateForwardMovement();
        AutoShoot();
    }

    float lockedOnTargetTime;
    GameObject lockedOnTarget;
    public void AutoShoot()
    {
        if (Time.time < readyToShoot) { return; }



        RaycastHit hit;
        Physics.Raycast(transform.position, (currentAim - transform.position).normalized, out hit, 1000);
        Debug.DrawRay(transform.position - Vector3.down, (currentAim - transform.position).normalized, Color.blue, 0.1f);

        bool shot = false;
        GameObject thisOne = hit.collider.gameObject;
        if (hit.collider != null && hit.collider.gameObject.CompareTag("Target"))
        {

            if (thisOne != lockedOnTarget)
            {
                // got a new one
                lockedOnTargetTime = Time.time; // start locking
                lockedOnTarget = thisOne;

            }
            else
            {
                if (Time.time - lockedOnTargetTime > 0.05f) // on target for a while
                {
                    hit.collider.gameObject.GetComponent<Target>().TakeDamage(damage);
                    shot = true;

                }
                else
                {
                    // focus on target maybe?
                }
            }


        }

        if (shot == true)
        {
            transform.position -= transform.forward * 1;
            GetComponent<Rigidbody>().AddForce(-transform.forward * 1000);
            readyToShoot = Time.time + shootCooldown;
            Camera.main.GetComponent<CameraEffects>().CameraShake(1.03f);

            Vector3 dir = hit.point - transform.position;
            GameObject shotEffect = Instantiate(gunShotEffect, muzzle.transform.position, Quaternion.identity);

            shotEffect.transform.LookAt(shotEffect.transform.position + dir);
            GameObject impactEffect = Instantiate(bulletImpactEffect, currentAim, Quaternion.identity);
            impactEffect.transform.LookAt(impactEffect.transform.position - dir);
        }
    }

    private Vector3 currentAim = Vector3.forward * 10;
    public Vector3 CurrentAimDir
    {
        get { return currentAim; }
    }
    public void AimAt(Vector3 target)
    {
        if (disableControl) return; // probably for auto-aim
        currentAim = target;
    }
    void UpdateMovement()
    {
        //transform.LookAt(currentAimDir + transform.position);
        Utility.ControlRotation(gameObject, currentAim - transform.position, transform.forward, 1, 0.7f);
        transform.localPosition = Vector3.Lerp(transform.localPosition, initialLocalPos, 0.2f);
        Vector3 targetUp = Vector3.Cross(transform.forward, Vector3.right);
        Utility.ControlRotation(gameObject, targetUp, transform.up, 1, 0.7f);
        Utility.ControlPosition(gameObject, transform.parent.TransformPoint(initialLocalPos), transform.position, null, 500, true, false, 0);

        Debug.DrawRay(currentAim - Vector3.right * 50, Vector3.right * 100);
        Debug.DrawRay(currentAim - Vector3.up * 50, Vector3.up * 100);
        Debug.DrawRay(currentAim - Vector3.forward * 50, Vector3.forward * 100);
    }


    public enum PowerUps
    {
        AutoAim,
        Power,
        Freeze
    }
    public void StartPowerUp(PowerUps type)
    {
        switch (type)
        {
            case PowerUps.AutoAim:
                StopCoroutine("AutoAim");
                StartCoroutine(AutoAim(AdjustValues.autoAimDuration));
                break;
            case PowerUps.Power:
                StopCoroutine("Power");
                StartCoroutine(Power(AdjustValues.powerDuration, AdjustValues.powerDamageMultiplier));
                break;
            case PowerUps.Freeze:
                StopCoroutine("Freeze");
                StartCoroutine(Freeze(AdjustValues.freezeDuration * AdjustValues.freezeMultiplier, AdjustValues.freezeMultiplier));
                break;
        }
    }
    IEnumerator Freeze(float duration, float speedMultiplier)
    {
        float startTime = Time.time;
        float endTime = startTime + duration;

        while (Time.time < endTime)
        {
            fireRate = initialFireRate * (1 / speedMultiplier);
            shootCooldown = 1 / fireRate;
            //powerMultiplier = Mathf.Lerp(powerMultiplier, speedMultiplier, 0.1f);
            Time.timeScale = Mathf.Lerp(Time.timeScale, speedMultiplier, 0.1f);
            yield return new WaitForFixedUpdate();
            GameObject.FindGameObjectWithTag("EffectsCanvas").GetComponent<EffectsCanvas>().freeze.SetActive(true);
        }

        fireRate = initialFireRate;
        shootCooldown = 1 / fireRate;
        GameObject.FindGameObjectWithTag("EffectsCanvas").GetComponent<EffectsCanvas>().freeze.SetActive(false);
        //powerMultiplier = 1;
        Time.timeScale = 1;
        yield return null;
    }
    IEnumerator Power(float duration, float damageMultiplier)
    {
        float startTime = Time.time;
        float endTime = startTime + duration;

        while (Time.time < endTime)
        {
            damage = initialDamage * damageMultiplier;
            yield return new WaitForFixedUpdate();
            GameObject.FindGameObjectWithTag("EffectsCanvas").GetComponent<EffectsCanvas>().power.SetActive(true);
        }

        GameObject.FindGameObjectWithTag("EffectsCanvas").GetComponent<EffectsCanvas>().power.SetActive(false);
        damage = initialDamage;
        yield return null;
    }
    IEnumerator AutoAim(float duration)
    {
        print("coroutine start");
        float startTime = Time.time;
        disableControl = true;
        float endTime = startTime + duration;


        while (Time.time < endTime)
        {

            // auto lock on target
            /*
            GameObject[] circles = GameObject.FindGameObjectsWithTag("Circle");
            Circle closest = null;
            foreach(GameObject circle in circles)
            {
                if (closest == null) closest = circle.GetComponent<Circle>();
                if(circle.transform.position.z < closest.transform.position.z)
                {
                    if (circle.GetComponentInChildren<Target>() != null)
                    {
                        closest = circle.GetComponent<Circle>();
                        print("found closest: " + closest.gameObject);
                    }
                }
            }

            Target target = closest.GetComponentInChildren<Target>();
            */
            GameObject.FindGameObjectWithTag("EffectsCanvas").GetComponent<EffectsCanvas>().autoAim.SetActive(true);
            disableControl = true;
            GameObject[] targetsGO = GameObject.FindGameObjectsWithTag("Target");
            GameObject closestTarget = null;
            foreach (GameObject targetGO in targetsGO)
            {
                if (closestTarget == null) closestTarget = targetGO;
                if (targetGO.transform.position.z < closestTarget.transform.position.z)
                {
                    closestTarget = targetGO;
                }
            }
            if (closestTarget != null)
            {
                Target target = closestTarget.GetComponent<Target>(); // Target target = GameObject.FindGameObjectWithTag("Target").GetComponent<Target>();
                print("target: " + target.gameObject);
                Vector3 targetPos = target.transform.position;
                currentAim = targetPos;
            }
            yield return new WaitForFixedUpdate();
        }
        disableControl = false;
        print("coroutine end");

        GameObject.FindGameObjectWithTag("EffectsCanvas").GetComponent<EffectsCanvas>().autoAim.SetActive(false);
        yield return null;
    }
}
