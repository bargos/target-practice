﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{
    public int asasds = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public static byte[] ArrayConcat(byte[] arr0, byte[] arr1, int size)
    {
        byte[] output = new byte[size * size * 2];

        int sizeSqr = size * size;
        int j = 0;
        for (int i = 0; i < sizeSqr; i++)
        {
            output[j] = arr0[i];
            j++;
        }
        for (int i = 0; i < sizeSqr; i++)
        {
            output[j] = arr1[i];
            j++;
        }
        return output;
    }
    public static void ArraySeperate(byte[] input, out byte[] arr0, out byte[] arr1, int size)
    {
        int sizeSqr = size * size;
        byte[] arr0ToReturn = new byte[sizeSqr];
        byte[] arr1ToReturn = new byte[sizeSqr];

        int j = 0;
        for (int i = 0; i < sizeSqr; i++)
        {
            arr0ToReturn[i] = input[j];
            j++;
        }
        for (int i = 0; i < sizeSqr; i++)
        {
            arr1ToReturn[i] = input[j];
            j++;
        }

        arr0 = arr0ToReturn;
        arr1 = arr1ToReturn;
    }

    public static byte[] Serialize(byte[,] input, int size)
    {
        byte[] output = new byte[size * size];
        int i = 0;
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                output[i] = input[x, y];
                i++;
            }
        }
        return output;
    }

    public static byte[,] Deserialize(byte[] input, int size)
    {
        byte[,] output = new byte[size, size];
        int i = 0;
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                output[x, y] = input[i];
                i++;
            }
        }
        return output;
    }

    public static void Print2DArray(byte[,] toPrint, int size)
    {
        string output = "";
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                output += toPrint[x, y] + "\t";
            }
            output += "\n";
        }
        print(output);
    }
    public static byte[,] Copy2DArray(byte[,] toCopy, int size)
    {
        byte[,] toReturn = new byte[size, size];
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                toReturn[x, y] = toCopy[x, y];
            }
        }
        return toReturn;
    }

    public static GameObject FindChild(string name, GameObject parent)
    {
        Transform[] allChildren = parent.transform.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (name.Equals(child.gameObject.name) && !name.Equals(parent.name))
            {
                return child.gameObject;
            }
        }
        return null;
    }

    public static void ControlRotation(GameObject targetObject, Vector3 target, Vector3 forward, float multiplier = 1, float dragMultiplier = 0.7f)
    {
        Vector3 targetDelta = target;
        float angleDiff = Vector3.Angle(forward, targetDelta);
        Vector3 cross = Vector3.Cross(forward, targetDelta);
        Vector3 torque = Vector3.ClampMagnitude(cross * angleDiff * 19, 88);
        targetObject.GetComponent<Rigidbody>().angularVelocity *= dragMultiplier;
        if (torque.x == Mathf.Infinity) { return; }
        targetObject.GetComponent<Rigidbody>().AddTorque(torque * multiplier);
    }
    public static Vector3 ControlPosition(GameObject targetObject, Vector3 target, Vector3 selfPos, Rigidbody relativeTo = null, float multiplier = 1, bool ZBound = true, bool applyLift = false, float dampenRelativeVel = 1)
    {
        Vector3 delta = target - selfPos;
        if (ZBound == false) { delta.z = 0; }
        float deltaMag = delta.magnitude;
        Vector3 deltaNormalized = delta / deltaMag;
        if(deltaMag < 0.0001f)
        {
            deltaNormalized = Vector3.zero;
        }
        float forceMultiplier = Mathf.Clamp(deltaMag, 0, 100) * 10;
        //dampen velocity
        Vector3 relativeVelocity = targetObject.GetComponent<Rigidbody>().velocity;
        if (relativeTo != null) { relativeVelocity -= relativeTo.velocity; }
        Vector3 relativeVelocityRemover = -relativeVelocity * 5.5f * targetObject.GetComponent<Rigidbody>().mass * relativeVelocity.magnitude; //Vector3 relativeVelocityRemover = -relativeVelocity * 4.5f * targetObject.GetComponent<Rigidbody>().mass ; //2.5

        
        relativeVelocityRemover *= dampenRelativeVel;
        //relativeVelocityRemover += -relativeVelocity * 1.5f * targetObject.GetComponent<Rigidbody>().mass; //2.5
        //targetObject.GetComponent<Rigidbody>().velocity = targetObject.GetComponent<Rigidbody>().velocity - relativeVelocity / 10;
        Vector3 force = forceMultiplier * deltaNormalized * 5 + relativeVelocityRemover;
        targetObject.GetComponent<Rigidbody>().AddForceAtPosition(force, selfPos);
        //Debug.DrawRay(selfPos, force * 10, Color.grey);

        Vector3 liftForce = Vector3.zero;
        if (applyLift == true)
        {
            liftForce = -targetObject.GetComponent<Rigidbody>().mass * Physics.gravity;
            targetObject.GetComponent<Rigidbody>().AddForce(liftForce);
        }


        // return total applied force to revert sway
        return (force + liftForce);

    }
    public static Vector3 GetMousePos()
    {
        Plane plane = new Plane(Vector3.forward, Vector3.zero);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance = 0.1f;
        if (plane.Raycast(ray, out distance) || true)
        {
            Vector3 hitPoint = ray.GetPoint(distance);
            hitPoint.z = 0;
            return hitPoint;
        }
        Debug.DrawRay(ray.origin, ray.direction * distance * 10, Color.cyan, 0.1f);
        return Vector3.zero;
    }
    public static Vector3 ApplyLift(Rigidbody target)
    {
        Vector3 force = -target.mass * Physics.gravity;
        return force;
    }
    public static void ControlRotation(Rigidbody targetRB, Vector3 targetForwardWorld, Vector3 localForward, float multiplier = 1)
    {

        Vector3 forceArmDirWorld = targetRB.transform.TransformDirection(localForward);

        Vector3 forcePoint = targetRB.transform.position + forceArmDirWorld;


        Vector3 force = targetForwardWorld * (280 * multiplier);

        targetRB.AddForceAtPosition(force, forcePoint);
        targetRB.AddForce(-force);

    }

    public static void RecursiveCollisionIgnore(GameObject first, GameObject second)
    {
        foreach (Collider colFirst in first.GetComponentsInChildren<Collider>())
        {
            foreach (Collider colSecond in second.GetComponentsInChildren<Collider>())
            {
                Physics.IgnoreCollision(colFirst, colSecond);
            }
        }
    }

    public static T GetComponentInFirstChildren<T>(GameObject parent) where T : MonoBehaviour
    {
        foreach (T t in parent.GetComponentsInChildren<T>())
        {
            if (t.transform.parent == parent.transform)
            {
                return t;
            }
        }

        return null;
    }
}

public class MovementInfo
{
    public Vector3 velocity;
    public Vector3 pos;
    public Quaternion rot;
    public MovementInfo(GameObject copyFrom)
    {
        velocity = copyFrom.GetComponent<Rigidbody>().velocity;
        pos = copyFrom.transform.position;
        rot = copyFrom.transform.rotation;
    }
    public void Restore(GameObject toRestore)
    {
        toRestore.GetComponent<Rigidbody>().velocity = velocity;
        toRestore.transform.position = pos;
        toRestore.transform.rotation = rot;
    }
}
