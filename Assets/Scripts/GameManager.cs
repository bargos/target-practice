﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [HideInInspector]
    public GameStates gameState;
    public static GameManager instance;
    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start()
    {
        gameState = GameStates.MAIN_MENU;
    }

    public void startGameClicked()
    {
        WaveSpawner.instance.GenerateLevelExample();
        gameState = GameStates.IN_GAME;
        UIManager.instance.startGame();
    }
    
    public void gameOver()
    {
        if (gameState == GameStates.IN_GAME)
        {
            gameState = GameStates.MAIN_MENU;
            UIManager.instance.gameOver();
        }
    }
}
