﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSlider : MonoBehaviour {
    
    Slider slider;
    Text levelText0;
    Text levelText1;
    Weapon playerWeapon;

    public int GetCurrentLevel()
    {
        return int.Parse(levelText0.text);
    }

    bool initialized = false;
    // Use this for initialization
    void Start () {
        
    }
    void CheckAndInitialize()
    {
        if (initialized) { return; }
        else
        {
            slider = GetComponentInChildren<Slider>();
            levelText0 = GetComponentInChildren<Text>();
            levelText1 = GetComponentsInChildren<Text>()[1];
            playerWeapon = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Weapon>();
            initialized = true;
        }
    }

	public void UpdateSlider(float currentPercentage)
    {
        CheckAndInitialize();

        slider.value = currentPercentage;
    }
    public void UpdateSlider(float currentPercentage, int currentLevel)
    {
        CheckAndInitialize();
        levelText0.text = currentLevel.ToString();
        levelText1.text = (currentLevel + 1).ToString();

        slider.value = currentPercentage;
    }

    private void FixedUpdate()
    {
        
    }

    /*
    float FindPercentage()
    {
        
        GameObject startCircle = null;
        GameObject endCircle = null;

        foreach (GameObject circle in GameObject.FindGameObjectsWithTag("Circle"))
        {
            if (startCircle == null) { startCircle = circle; }
            if (endCircle == null) { endCircle = circle; }

            float circleZ = circle.transform.position.z;
            if (circleZ < startCircle.transform.position.z)
            {
                startCircle = circle;
            }
            if (circleZ > endCircle.transform.position.z)
            {
                endCircle = circle;
            }
        }


        float levelStart = startCircle.transform.position.z;
        float levelEnd = endCircle.transform.position.z;
        float playerPos = playerWeapon.transform.position.z;

        float percentage = Mathf.Clamp((playerPos - levelStart) / (levelEnd - levelStart), 0, 1);

        print("level progress: " + percentage);
        return percentage;
    }
    */
    
    
}
