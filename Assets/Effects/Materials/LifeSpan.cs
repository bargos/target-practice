﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeSpan : MonoBehaviour {
    public float lifeSpan = 1;

	void Start () {
        Destroy(gameObject, lifeSpan);
	}

}
